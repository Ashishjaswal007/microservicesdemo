﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroservicesDemo.DbContexts;
using MicroservicesDemo.Models;
using Microsoft.EntityFrameworkCore;

namespace MicroservicesDemo.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly ProductContext _dbContext;
        public ProductRepository(ProductContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void DeleteProduct(int productId)
        {
            var Id = _dbContext.Products.Find(productId);
            _dbContext.Remove(Id);
            Save();
        }

        public Product GetProductByID(int product)
        {
            var productId = _dbContext.Products.Find(product);
            return productId;
        }

        public IEnumerable<Product> GetProducts()
        {
            var list = _dbContext.Products.ToList();
            return list;

        }

        public void InsertProduct(Product product)
        {
            _dbContext.Products.Add(product);
            //Save();
        }

        public void Save()
        {
            _dbContext.SaveChanges();

        }

        public void UpdateProduct(Product product)
        {
            _dbContext.Entry(product).State = EntityState.Modified;
            Save();
        }
    }
}
