﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroservicesDemo.Models;
using MicroservicesDemo.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MicroservicesDemo.Controllers
{



    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepository _productRepository;

        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        // GET: api/Product
        [HttpGet]
        public IActionResult GetAll()
        {
            var product = _productRepository.GetProducts();
            return new OkObjectResult(product);

        }

        // GET: api/Product/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult GetById(int id)
        {
            var product = _productRepository.GetProductByID(id);
            return new OkObjectResult(product);
        }

        // POST: api/Product
        [HttpPost]
        public IActionResult AddEdit([FromBody] Product Model)
        {
            if (Model.Id == 0)
            {
                _productRepository.InsertProduct(Model);
            }
            var obj = _productRepository.GetProductByID(Model.Id);
            if (obj.Id>0)
            {
                obj.Name = Model.Name;
                obj.Description = Model.Description;
                obj.Price = Model.Price;
                obj.CategoryId = Model.CategoryId;
            }
            _productRepository.Save();
             return CreatedAtAction(nameof(GetById), new { id = Model.Id }, Model);
            //return new OkResult();
        }

        // PUT: api/Product/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _productRepository.DeleteProduct(id);
            return new OkResult();

        }
    }
}
